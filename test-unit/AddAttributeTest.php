<?php

namespace Pantagruel74\Yii2DynamicModelAddAttributeTestUnit;

use Pantagruel74\Yii2DynamicModelAddAttributeStubs\AddAttributeModelStub;
use Pantagruel74\Yii2Loader\Yii2Loader;
use PHPUnit\Framework\TestCase;
use yii\base\DynamicModel;

class AddAttributeTest extends TestCase
{
    /**
     * @param string|null $name
     * @param array $data
     * @param $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        Yii2Loader::load();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @return void
     */
    public function testAdding(): void
    {
        $model = new AddAttributeModelStub();
        $model->addAttribute('attr1', 'attr1val', 'Attr 1');
        $model->addAttribute('2', 'attr2val', 'Attr 2');
        $this->assertEquals([
            'dmattr1' => 'attr1val',
            'dm2' => 'attr2val'
        ], $model->getAttributes());
        $this->assertEquals([
            'dmattr1' => 'Attr 1',
            'dm2' => 'Attr 2'
        ],$model->attributeLabels());
        $this->assertEquals(['dmattr1', 'dm2'], $model->safeAttributes());
    }
}