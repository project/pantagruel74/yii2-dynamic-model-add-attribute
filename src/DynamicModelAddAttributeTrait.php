<?php

namespace Pantagruel74\Yii2DynamicModelAddAttribute;

use yii\base\DynamicModel;

trait DynamicModelAddAttributeTrait
{
    /**
     * @return string
     */
    abstract public static function namePrefix(): string;

    /**
     * @param string $name
     * @param $value
     * @param string $label
     * @return void
     */
    public function addAttribute(string $name, $value, string $label): void
    {
        /* @var DynamicModel $this */
        $name = self::productAttributeName($name);
        $this->defineAttribute($name, $value);
        $this->setAttributeLabel($name, $label);
        $this->addRule($name, 'safe');
    }

    /**
     * @param string $name
     * @return string
     */
    protected static function productAttributeName(string $name): string
    {
        return static::namePrefix() . preg_replace( "/[^a-zA-ZА-Яа-я0-9\s]/", '', $name);
    }

    /*
     *
     */
    public function getAttribute(string $name)
    {
        $name = self::productAttributeName($name);
        return $this->$name;
    }
}