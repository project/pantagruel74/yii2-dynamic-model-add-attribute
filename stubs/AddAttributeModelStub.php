<?php

namespace Pantagruel74\Yii2DynamicModelAddAttributeStubs;

use Pantagruel74\Yii2DynamicModelAddAttribute\DynamicModelAddAttributeTrait;
use yii\base\DynamicModel;
use yii\base\Model;

class AddAttributeModelStub extends DynamicModel
{
    use DynamicModelAddAttributeTrait;

    public static function namePrefix(): string
    {
        return 'dm';
    }
}